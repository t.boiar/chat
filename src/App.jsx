import { BrowserRouter, Routes, Route } from "react-router-dom";
import ChatPage from "./ChatPage";
import LoginPage from "./LoginPage";

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" index element={<LoginPage />} />
        <Route path="chat" element={<ChatPage />} />
      </Routes>
    </BrowserRouter>
  );
}
