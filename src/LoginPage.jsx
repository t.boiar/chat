import { useEffect, useState } from "react";
import jwtDecode from "jwt-decode";
import { Navigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";

export default function LoginPage() {
  const [user, setUser] = useState(localStorage.getItem("user") || false);

  function handleCallbackResponse(repsonse) {
    const userObject = jwtDecode(repsonse.credential);
    setUser(userObject);
    localStorage.setItem("user", JSON.stringify(userObject));
  }

  useEffect(() => {
    try {
      /* global google */
      google.accounts.id.initialize({
        client_id:
          "988121643890-3idsa6h6lg9e3dsonfmf3qv2n9lr1lmf.apps.googleusercontent.com",
        callback: handleCallbackResponse,
      });

      google.accounts.id.renderButton(document.getElementById("signInDiv"), {
        theme: "outline",
        size: "large",
      });
      google.accounts.id.prompt();
    } catch (e) {
      toast(`Something went wrong with Google auth. Please, login as guest 🕵️`);
    }
  }, []);

  function signInAsGuest() {
    setUser({ name: "Guest" });
    localStorage.setItem("user", JSON.stringify({ name: "Guest" }));
  }

  if (user) {
    return <Navigate to="/chat" />;
  }
  return (
    <div className="login-page">
      <div className="title-container">
        <img className="chat-icon" src="/assets/chat.svg" />
        <h1 className="login-title">Chat</h1>
      </div>
      <div className="login-container">
        <div id="signInDiv"></div>
        <div onClick={signInAsGuest} className="guest-link tooltip">
          <span>
            <img className="guest-img" src="/assets/incognito.svg" />
          </span>
          <span class="tooltiptext">Login as guest</span>
        </div>
      </div>
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover={false}
      />
    </div>
  );
}
