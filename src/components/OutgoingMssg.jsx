import React from "react";
import moment from "moment";

export default function OutgoingMssg(props) {
  return (
    <div className="chat-message chat-message__outgoing">
      <p className="message outgoing-message">{props.text}</p>
      <p className="message-time">
        {moment.unix(props.date).format("M/D/YY, LT")}
      </p>
    </div>
  );
}
