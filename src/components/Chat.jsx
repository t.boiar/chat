import moment from "moment";
import React, { useState, useEffect } from "react";
import IncomingMssg from "./IncomingMssg";
import OutgoingMssg from "./OutgoingMssg";

import { toast } from "react-toastify";

export default function Chat(props) {
  const [mssg, setNewMssg] = useState("");

  function sendMessg(e) {
    e.preventDefault();
    if (mssg !== "" && mssg !== " ") {
      const newOutMssg = {
        author_id: 0,
        text: mssg,
        date: moment().unix(),
      };
      props.updateMssgs(props.id, newOutMssg);
      setNewMssg("");
      setTimeout(() => {
        fetch("https://api.chucknorris.io/jokes/random")
          .then((res) => res.json())
          .then((mssgsData) => {
            const newIncomMssg = {
              author_id: props.id,
              text: mssgsData.value,
              date: moment().unix(),
            };
            props.updateMssgs(props.id, newIncomMssg);
          });
        toast(`New message from ${props.name}!`);
      }, 10000);
    }
  }

  const messages = props.messages.map((messg, index) => {
    if (messg.author_id === 0) {
      return <OutgoingMssg key={index} {...messg} />;
    } else {
      return <IncomingMssg key={index} {...messg} img={props.profileImg} />;
    }
  });

  function handleChange(event) {
    const { value } = event.target;
    setNewMssg(value);
  }

  return (
    <div className="chat">
      <header className="chat-header">
        <div className="profile-container">
          <img
            className="profile-img"
            src={props.profileImg}
            alt="Profile image"
          />
          <span className="profile-status"></span>
        </div>
        <h1 className="chat-title">{props.name}</h1>
      </header>
      <div className="chat-list">{messages}</div>
      <div className="form-container">
        <form className="chat-form" onSubmit={sendMessg}>
          <input
            className="message-input"
            type="text"
            placeholder="Type your message"
            name="text"
            maxLength="600"
            value={mssg}
            onChange={handleChange}
          />
          <span className="message-send" onClick={sendMessg}></span>
        </form>
      </div>
    </div>
  );
}
