import React from "react";
import moment from "moment";

export default function ContactCard(props) {
  return (
    <div
      data-id={props.id}
      className={props.activeCard === props.id ? "card active-card" : "card"}
      onClick={props.handleClick}
    >
      <div className="profile-container">
        <img
          className="card-img profile-img"
          src={props.profileImg}
          alt="Profile image"
        />
        <span className="profile-status"></span>
      </div>
      <div className="card-data">
        <div>
          <h3 className="card-name">{props.name}</h3>
          <p className="card-date">
            {moment
              .unix(props.messages[props.messages.length - 1].date)
              .format("MMM D, YYYY")}
          </p>
        </div>
        <p className="card-message">
          {props.messages[props.messages.length - 1].text}
        </p>
      </div>
    </div>
  );
}
