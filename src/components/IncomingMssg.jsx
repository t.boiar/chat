import React from "react";
import moment from "moment";

export default function IncomingMssg(props) {
  return (
    <div className="chat-message chat-message__incoming">
      <img className="profile-img" src={props.img} alt="Profile image" />
      <div>
        <p className="incoming-message message">{props.text}</p>
        <p className="message-time">
          {moment.unix(props.date).format("M/D/YY, LT")}
        </p>
      </div>
    </div>
  );
}
