export default [
  {
    id: 1,
    name: "Márquez",
    profileImg: "/assets/profile-imgs/marquez.jpg",
    messages: [
      {
        author_id: 1,
        text: "Sit cillum nulla est sit nisi laborum velit non nisi.",
        date: 1495147662.0,
      },
      {
        author_id: 0,
        text: "Laboris ea cupidatat consequat occaecat do in dolore aliquip sit laboris elit.",
        date: 1574327352.0,
      },
      {
        author_id: 1,
        text: "What matters in life is not what happens to you but what you remember and how you remember it",
        date: 1610506942.0,
      },
    ],
  },
  {
    id: 2,
    name: "Donkey",
    profileImg: "/assets/profile-imgs/donkey.jpg",
    messages: [
      {
        author_id: 0,
        text: "Laboris ea cupidatat consequat occaecat do in dolore aliquip sit laboris elit.",
        date: 1561453001.0,
      },
      {
        author_id: 2,
        text: "Maybe I'll see y'all Sunday for a barbecue of something.",
        date: 1479098137.0,
      },
    ],
  },
  {
    id: 3,
    name: "Virginia Woolf",
    profileImg: "/assets/profile-imgs/woolf.jpeg",
    messages: [
      {
        author_id: 3,
        text: "Ut esse est minim Lorem.",
        date: 1494923350.0,
      },
      {
        author_id: 0,
        text: "Veniam minim ullamco esse do eu aliquip est veniam fugiat.",
        date: 1525757641.0,
      },
      {
        author_id: 3,
        text: "Voluptate nisi ipsum minim est labore exercitation commodo laborum qui ea aute esse culpa laborum.",
        date: 1607901043.0,
      },
      {
        author_id: 0,
        text: "You cannot find peace by avoiding life",
        date: 1660904424.0,
      },
    ],
  },
  {
    id: 4,
    name: "Elvis Presley",
    profileImg: "/assets/profile-imgs/presley.jpg",
    messages: [
      {
        author_id: 0,
        text: "Id est veniam exercitation excepteur.",
        date: 1575963522.0,
      },
      {
        author_id: 4,
        text: "Irure pariatur aute ea laborum velit ullamco occaecat reprehenderit aliqua occaecat sit.",
        date: 1644984559.0,
      },
      {
        author_id: 0,
        text: "If you let your head get too big, it’ll break your neck.",
        date: 1646786903.0,
      },
    ],
  },
  {
    id: 5,
    name: "Maria Prymachenko",
    profileImg: "/assets/profile-imgs/prymachenko.jpg",
    messages: [
      {
        author_id: 5,
        text: "I make sunny flowers because I love people, I make them for people's happiness so that my flowers are like the life of the people so that all peoples love each other so that people live like flowers bloom all over the earth.",
        date: 1551054367.0,
      },
    ],
  },
  {
    id: 6,
    name: "Gandalf",
    profileImg: "/assets/profile-imgs/gandalf.jpg",
    messages: [
      {
        author_id: 0,
        text: "Dolor consequat proident mollit quis nulla sunt excepteur consectetur sint Lorem aliqua est officia fugiat.",
        date: 1545974091.0,
      },
      {
        author_id: 6,
        text: "Mollit nostrud sint cillum reprehenderit ad esse dolor qui anim commodo.",
        date: 1566007362.0,
      },
      {
        author_id: 0,
        text: "Cillum magna anim sint irure irure laborum ad id duis amet laboris incididunt quis qui.",
        date: 1577094666.0,
      },
      {
        author_id: 0,
        text: "You shall not pass!",
        date: 1593667225.0,
      },
    ],
  },
  {
    id: 7,
    name: "Amy Winehouse",
    profileImg: "/assets/profile-imgs/winehouse.jpeg",
    messages: [
      {
        author_id: 0,
        text: "Duis nostrud ea mollit tempor ea culpa culpa.",
        date: 1414568673.0,
      },
      {
        author_id: 7,
        text: "Nisi do commodo nisi in adipisicing.",
        date: 1428651443.0,
      },
      {
        author_id: 0,
        text: "Sit aliqua dolore ut quis ullamco minim laborum ex fugiat voluptate.",
        date: 1461643915.0,
      },
      {
        author_id: 7,
        text: "I'll go back to black...",
        date: 1658969171.0,
      },
    ],
  },
  {
    id: 8,
    name: "Aristotle",
    profileImg: "/assets/profile-imgs/aristotle.jpeg",
    messages: [
      {
        author_id: 8,
        text: "Ea ullamco excepteur qui minim minim in.",
        date: 1551054367.0,
      },
      {
        author_id: 0,
        text: "Mollit nisi ad anim enim cupidatat Lorem quis do mollit nulla nulla ullamco incididunt.",
        date: 1604823063.0,
      },
      {
        author_id: 8,
        text: "Ipsum dolore consequat reprehenderit voluptate sit cupidatat ea aliquip magna ut esse qui.",
        date: 1637048859.0,
      },
      {
        author_id: 8,
        text: "Pleasure in the job puts perfection in the work.",
        date: 1654756116.0,
      },
    ],
  },
  {
    id: 9,
    name: "Kevin McCallister",
    profileImg: "/assets/profile-imgs/kevin.jpeg",
    messages: [
      {
        author_id: 0,
        text: "Laboris ea cupidatat consequat occaecat do in dolore aliquip sit laboris elit.",
        date: 1574327352.0,
      },
      {
        author_id: 9,
        text: "I'm eating junk and watching rubbish!",
        date: 1610506942.0,
      },
    ],
  },
  {
    id: 10,
    name: "Sir Isaac Newton",
    profileImg: "/assets/profile-imgs/newton.jpg",
    messages: [
      {
        author_id: 10,
        text: "You are the rock!",
        date: 1610506942.0,
      },
    ],
  },
];
