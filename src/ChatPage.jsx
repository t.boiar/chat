import React, { useState, useEffect } from "react";
import Chat from "./components/Chat";
import ContactCard from "./components/ContactCard";
import contactData from "./contactData";
import { Navigate } from "react-router-dom";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function ChatPage() {
  const sortContacts = (a, b) => {
    return (
      b.messages[b.messages.length - 1].date -
      a.messages[a.messages.length - 1].date
    );
  };

  const [chat, setChat] = useState(false);

  const [name, setName] = useState("");
  const [user, setUser] = useState(
    () => JSON.parse(localStorage.getItem("user")) || false
  );

  const defaultChatData =
    JSON.parse(localStorage.getItem("chatData")) || contactData;

  const [contactCards, setContactCards] = useState(defaultChatData);

  function openChat(event) {
    const cardId = Number.parseInt(event.currentTarget.dataset.id);
    const found = contactCards.find(({ id }) => id === cardId);
    setChat(found);
  }

  const updateChatMssgs = (id, mssg) => {
    setContactCards((prevCards) => {
      const newCards = [...prevCards];
      const foundChat = newCards.find((el) => el.id === id);
      foundChat && foundChat.messages.push(mssg);
      if (!localStorage.hasOwnProperty("chatData")) {
        localStorage.setItem("chatData", JSON.stringify(defaultChatData));
      }
      const chatsFromLS = JSON.parse(localStorage.getItem("chatData"));

      chatsFromLS.find((el) => el.id === id).messages.push(mssg);
      localStorage.setItem("chatData", JSON.stringify(chatsFromLS));
      return newCards;
    });
  };

  const cards = contactCards.sort(sortContacts).map((card) => {
    return (
      <ContactCard
        activeCard={chat.id}
        key={card.id}
        {...card}
        handleClick={openChat}
      />
    );
  });

  const searchContact = (e) => {
    const keyword = e.target.value;

    if (keyword !== "") {
      const results = defaultChatData.filter((card) => {
        return card.name.toLowerCase().includes(keyword.toLowerCase());
      });
      setContactCards(results);
    } else {
      setContactCards(defaultChatData);
    }
    setName(keyword);
  };

  function handleSignOut(event) {
    localStorage.removeItem("user");
    setUser(false);
  }
  if (!user) {
    return <Navigate to="/" />;
  }

  return (
    <div className="ChatPage">
      <div className="contacts">
        <header className="contacts-header">
          <div className="login-profile">
            <div className="profile-container">
              <img
                className="profile-img"
                src={user.picture || "/assets/profile-imgs/empty.png"}
                alt="Profile image"
              />
              <span className="profile-status"></span>
            </div>
            <div className="sing-out" onClick={(e) => handleSignOut(e)}></div>
          </div>
          <div className="search-container">
            <span className="search-icon"></span>
            <input
              className="search-input"
              type="text"
              placeholder="Search or start new chat"
              value={name}
              onChange={searchContact}
            />
          </div>
        </header>
        <div className="contacts-list">
          <h2 className="contacts-title">Chats</h2>
          <div className="contacts-cards">{cards}</div>
        </div>
      </div>
      {chat && <Chat key={chat.id} {...chat} updateMssgs={updateChatMssgs} />}
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover={false}
      />
    </div>
  );
}

export default ChatPage;
